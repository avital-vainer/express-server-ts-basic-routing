import express, { NextFunction } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRouter from "./routers/users.js";
import webRouter from "./routers/web.js";


const { PORT, HOST } = process.env;

const app = express();

 
app.use( morgan('dev') );
app.use( express.json() );
app.use("/users", usersRouter);
app.use("/", webRouter);


app.listen(Number(PORT), HOST as string,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});
