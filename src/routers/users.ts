import express, { Router, Request, Response, NextFunction, RequestHandler } from 'express';
import log from '@ajar/marker';

const router: Router = express.Router();
export default router;


const myLog: RequestHandler = (req, res, next) => {
    log.info(`request was called... ${req.url}`);
    next();
}

// const paymentRequired = (req: Request, res: Response, next: NextFunction) => {
//     console.log();
//     next();
// }

router.get('/',  (req: Request, res: Response) => {
    res.status(200).send('Get all Users');
});

router.get('/search', (req: Request, res: Response) => {
    // http://localhost:3030/users/search?name=Avi&town=Ashdod
    res.status(200).json(req.query);
});

router.get('/:userName/:userCity', (req: Request, res: Response) => {
    // http://localhost:3030/users/Moshe/Tel-Aviv
    const htmlResponse = `<h1> 👩‍💼  <strong>Users</strong>  🧑‍💼 </h1>
                          <h3> ${req.params.userName} lives in ${req.params.userCity} </h3>`;
    res.status(200).set('Content-Type', 'text/html').send(htmlResponse);
});