import express, { Router, Request, Response, NextFunction } from 'express';
import log from '@ajar/marker';

const router: Router = express.Router();
export default router;


router.get('/',  (req: Request, res: Response) => {
    res.status(200).send('Hello Express!');
});

router.get('/login', (req: Request, res: Response) => {
    // http://localhost:3030/login
    res.status(200).send("Please Login");
});

router.use('*',  (req: Request, res: Response, next: NextFunction) => {
    const curUrl = new URL(req.url, `http://${req.headers.host}`);
    const htmlResponse = `<h2> 404 - url ${curUrl.href + req.params['0'].substr(1)} was not found 😔 </h2>`;
    res.status(404).set('Content-Type', 'text/html').send(htmlResponse);
});
